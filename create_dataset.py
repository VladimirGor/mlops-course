import pandas as pd

def create_data(train_file_path, test_file_path):
    train = pd.read_csv(train_file_path, delimiter=',', parse_dates=['date'],
                        infer_datetime_format=True)
    train.sort_values(by=['date'], inplace=True, ascending=True)
    train["item_cnt_day"] = train.item_cnt_day.replace(
        {-1: 0, -2: 0, -3: 0, -6: 0, -5: 0, -4: 0, -22: 0, -16: 0, -9: 0})
    # Создаем столбец с правильным значениями отсчета месяцев,
    #  тк date_block_num с ошибками
    train['date_block_num'] = (train['date'].dt.to_period('M').astype(
        'int') - pd.to_datetime(['2013-01-01']).to_period('M').astype('int'))
    dataset = pd.pivot_table(train, index=['shop_id', 'item_id'], values=[
                             'item_cnt_day'], columns=['date_block_num'],
                             fill_value=0, aggfunc='sum')
    dataset.reset_index(inplace=True)
    test = pd.read_csv(test_file_path, delimiter=',')
    test = test.drop(['ID'], axis=1)
    dataset = pd.merge(test, dataset, on=['item_id', 'shop_id'], how='left')
    dataset = dataset.fillna(dataset.mean())
    dataset.drop(['shop_id', 'item_id'], inplace=True, axis=1)
    dataset.columns = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                       '10', '11', '12', '13', '14', '15', '16', '17',
                       '18', '19', '20', '21', '22', '23', '24', '25',
                       '26', '27', '28', '29', '30', '31', '32', '33',
                       '34', '35']
    return dataset
